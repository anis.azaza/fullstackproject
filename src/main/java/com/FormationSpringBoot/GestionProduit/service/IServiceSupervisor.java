package com.FormationSpringBoot.GestionProduit.service;

import java.util.List;

import com.FormationSpringBoot.GestionProduit.entities.Departement;
import com.FormationSpringBoot.GestionProduit.entities.Supervisor;


public interface IServiceSupervisor {
	public void saveSupervisor(Supervisor supervisor);
	List<Supervisor>getAllSupervisor();
	Departement getDepartementBySupervisor(int idsup);

}
