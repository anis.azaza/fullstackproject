package com.FormationSpringBoot.GestionProduit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FormationSpringBoot.GestionProduit.dao.EmployeeRepository;
import com.FormationSpringBoot.GestionProduit.entities.Employee;

import lombok.AllArgsConstructor;


@Service
@AllArgsConstructor
public class ServiceEmployeeImp implements IServiceEmployee {

	@Autowired
	EmployeeRepository er;
	
	@Override
	public void saveEmployee(Employee employee) {
		// TODO Auto-generated method stub
		er.save(employee);
		
	}


	@Override
	public List<Employee> getAllEmployees() {
		// TODO Auto-generated method stub
		return er.findAll();
	}

	@Override
	public Employee getEmployee(int idemp) {
		// TODO Auto-generated method stub
		return er.findById(idemp).get();
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return er.save(employee);
	}

	@Override
	public void deleteEmployee(int idemp) {
		// TODO Auto-generated method stub
		er.deleteById(idemp);
		
	}


	@Override
	public List<Employee> getEmpByDep(int ideep) {
		// TODO Auto-generated method stub
		return er.getEmpByDepId(ideep);
	}

	
}
