package com.FormationSpringBoot.GestionProduit.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FormationSpringBoot.GestionProduit.dao.SkillsRepository;
import com.FormationSpringBoot.GestionProduit.entities.Employee;
import com.FormationSpringBoot.GestionProduit.entities.Skills;

import lombok.AllArgsConstructor;



@Service
@AllArgsConstructor
public class ServiceSkillImp implements IServiceSkills {
	
	@Autowired
	SkillsRepository sr;

	@Override
	public void saveSkill(Skills skill) {
		// TODO Auto-generated method stub
		sr.save(skill);
	}

	@Override
	public List<Skills> getAllSkills() {
		// TODO Auto-generated method stub
		return sr.findAll();
	}

	@Override
	public List<Employee> getEmployeesBySkill(int idsk) {
		// TODO Auto-generated method stub
		return sr.getById(idsk).getEmployeesskilled();
	}

	


}
