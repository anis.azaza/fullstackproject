package com.FormationSpringBoot.GestionProduit.service;

import java.util.List;
import java.util.Set;

import com.FormationSpringBoot.GestionProduit.entities.Employee;
import com.FormationSpringBoot.GestionProduit.entities.Skills;


public interface IServiceSkills {
	public void saveSkill(Skills skill);
	List<Skills>getAllSkills();
	List<Employee> getEmployeesBySkill(int idsk);

}
