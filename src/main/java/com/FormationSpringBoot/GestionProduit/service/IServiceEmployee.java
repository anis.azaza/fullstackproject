package com.FormationSpringBoot.GestionProduit.service;

import java.util.List;

import com.FormationSpringBoot.GestionProduit.entities.Employee;


public interface IServiceEmployee {
	public void saveEmployee(Employee employee);
	List<Employee> getAllEmployees();
	Employee getEmployee(int idemp);
	Employee updateEmployee(Employee employee);
	public void deleteEmployee(int idemp);
	List<Employee> getEmpByDep(int ideep);
}
