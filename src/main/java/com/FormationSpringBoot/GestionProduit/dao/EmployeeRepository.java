package com.FormationSpringBoot.GestionProduit.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.FormationSpringBoot.GestionProduit.entities.Employee;


@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	@Query("SELECT u FROM Employee u WHERE u.departement.id = ?1")
	List<Employee> getEmpByDepId(int id);
	

}
