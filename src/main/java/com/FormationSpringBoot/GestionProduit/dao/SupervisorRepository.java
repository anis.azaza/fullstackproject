package com.FormationSpringBoot.GestionProduit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.FormationSpringBoot.GestionProduit.entities.Departement;
import com.FormationSpringBoot.GestionProduit.entities.Supervisor;


@Repository
public interface SupervisorRepository extends JpaRepository<Supervisor, Integer>{
	 

}
