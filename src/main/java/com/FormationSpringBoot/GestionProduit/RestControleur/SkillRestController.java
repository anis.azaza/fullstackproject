package com.FormationSpringBoot.GestionProduit.RestControleur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.FormationSpringBoot.GestionProduit.entities.Skills;
import com.FormationSpringBoot.GestionProduit.service.IServiceSkills;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/apisskill")
public class SkillRestController {
	
	@Autowired
	IServiceSkills ss;
	
	@GetMapping("/all")
	public List<Skills> getAllSkills(){
		return ss.getAllSkills();
	}		
	
	@PostMapping("/add")
	public void addSkill (@RequestParam("skill") String p) throws JsonMappingException, JsonProcessingException{
		Skills skill = new ObjectMapper().readValue(p, Skills.class);
		ss.saveSkill(skill);;
	}
	
	@PostMapping("/add1")
	public void addSupervisorr(@RequestBody Skills sup) {
		ss.saveSkill(sup);
	}
	
	}


