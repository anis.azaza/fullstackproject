package com.FormationSpringBoot.GestionProduit.RestControleur;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.FormationSpringBoot.GestionProduit.dao.EmployeeRepository;
import com.FormationSpringBoot.GestionProduit.entities.Employee;
import com.FormationSpringBoot.GestionProduit.entities.Supervisor;
import com.FormationSpringBoot.GestionProduit.service.IServiceEmployee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/apiemp")
public class EmployeeRestController {
	
	@Autowired
	IServiceEmployee se;
	@Autowired
	EmployeeRepository er;
	
	@GetMapping("/all")
	public List<Employee> getAllEmployees(){
		return se.getAllEmployees();
	}		
	
	@PostMapping("/add")
	public void addEmployee (@RequestParam("Employee") String p) throws JsonMappingException, JsonProcessingException{
		Employee emp = new ObjectMapper().readValue(p, Employee.class);
		se.saveEmployee(emp);
	}
	
	@PostMapping("/add1")
	public void addEmp(@RequestBody Employee sup) {
		se.saveEmployee(sup);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteEmp(@PathVariable int id) throws IOException{
		se.deleteEmployee(id);
	}

	@PutMapping("/update")
	public void update(@RequestParam("Employee") String p) throws JsonMappingException, JsonProcessingException{
		addEmployee(p);
	}
	
	@GetMapping("/emp/{id}")
	public Employee getEmployeeById (@PathVariable int id) {
		return se.getEmployee(id);
	}
	
	@GetMapping("/empbydep/{id}")
	public List<Employee> getEmpByDep(@PathVariable int id){
		return se.getEmpByDep(id);
	}
	
	@GetMapping("/emp/count")
	private Long getNumberOfUsers(){
	    return er.count();
	}

}
