package com.FormationSpringBoot.GestionProduit.RestControleur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.FormationSpringBoot.GestionProduit.dao.DepartementRepository;
import com.FormationSpringBoot.GestionProduit.entities.Departement;
import com.FormationSpringBoot.GestionProduit.entities.Employee;
import com.FormationSpringBoot.GestionProduit.entities.Produit;
import com.FormationSpringBoot.GestionProduit.entities.Supervisor;
import com.FormationSpringBoot.GestionProduit.service.IServiceDepartement;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/apidep")
public class DepartementRestController {
	
	@Autowired
	IServiceDepartement sd;
	@Autowired
	DepartementRepository dr;
	
	@GetMapping("/all")
	public List<Departement> getAllDepartements(){
	return sd.allDepartements();
	}
	
	@PostMapping("/add")
	public void addDep (@RequestParam("departement") String p) throws JsonMappingException, JsonProcessingException{
		Departement dep = new ObjectMapper().readValue(p, Departement.class);
		sd.addDepartement(dep);;
	}
	
	@PostMapping("/add1")
	public void addDepp(@RequestBody Departement dep) {
		sd.addDepartement(dep);
	}
	
	@GetMapping("/depbysup/{id}")
	public Departement getDepBySup(@PathVariable int id){
	return sd.getDepBySupervisor(id);
	}
	
	@GetMapping("/dep/count")
	private Long getNumberOfUsers(){
	    return dr.count();
	}

}
