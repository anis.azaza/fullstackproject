package com.FormationSpringBoot.GestionProduit.RestControleur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.FormationSpringBoot.GestionProduit.dao.SupervisorRepository;
import com.FormationSpringBoot.GestionProduit.entities.Departement;
import com.FormationSpringBoot.GestionProduit.entities.Supervisor;
import com.FormationSpringBoot.GestionProduit.service.IServiceSupervisor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/apisupervisor")
public class SupervisorRestController {
	
	@Autowired
	private IServiceSupervisor ss;
	@Autowired
	SupervisorRepository sr;
	
	
	@GetMapping("/all")
	public List<Supervisor> getAllSupervisors(){
		return ss.getAllSupervisor();
	}		
	
	@PostMapping("/add")
	public void addSupervisor (@RequestParam("supervisor") String p) throws JsonMappingException, JsonProcessingException{
		Supervisor sup = new ObjectMapper().readValue(p, Supervisor.class);
		ss.saveSupervisor(sup);
	}
	
	@PostMapping("/add1")
	public void addSupervisorr(@RequestBody Supervisor sup) {
		ss.saveSupervisor(sup);
	}
	
	@GetMapping("/depbysup/{id}")
	public Departement dep (@PathVariable int id){
		return ss.getDepartementBySupervisor(id);
	}
	
	@GetMapping("/sup/count")
	private Long getNumberOfUsers(){
	    return sr.count();
	}
	
	}
	



