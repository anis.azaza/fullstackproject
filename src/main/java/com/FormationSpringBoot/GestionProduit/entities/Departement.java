package com.FormationSpringBoot.GestionProduit.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Entity
@AllArgsConstructor @NoArgsConstructor
public class Departement {
	@Id @GeneratedValue (strategy=GenerationType.IDENTITY)
	private Integer id;
	private String name;
	@OneToMany(mappedBy = "departement",cascade=CascadeType.ALL)
	@JsonIgnore
	private List<Employee>listemp;
	@OneToOne
	private Supervisor supervisor;
	
	

}
