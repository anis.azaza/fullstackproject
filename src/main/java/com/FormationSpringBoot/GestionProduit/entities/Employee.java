package com.FormationSpringBoot.GestionProduit.entities;

import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Entity
@AllArgsConstructor @NoArgsConstructor
public class Employee {
	@Id @GeneratedValue (strategy=GenerationType.IDENTITY)
	private Integer id;
	private String fname;
	private String lname;
	private String email;
	private int Age;
	@ManyToOne
	private Departement departement;
	@ManyToMany
	private List<Skills> skillshave;

}
