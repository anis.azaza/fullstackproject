package com.FormationSpringBoot.GestionProduit.entities;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Entity
@AllArgsConstructor @NoArgsConstructor
public class Supervisor {
	@Id @GeneratedValue (strategy=GenerationType.IDENTITY)
	private Integer id;
	private String fname;
	private String lname;
	@OneToOne(mappedBy = "supervisor",cascade=CascadeType.ALL)
	@JsonIgnore
	private Departement departement;

}
