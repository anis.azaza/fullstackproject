package com.FormationSpringBoot.GestionProduit.entities;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Entity
@AllArgsConstructor @NoArgsConstructor
public class Skills {
	@Id @GeneratedValue (strategy=GenerationType.IDENTITY)
	private Integer id;
	private String name;
	@ManyToMany(mappedBy="skillshave",cascade =CascadeType.ALL)
	@JsonIgnore
	private List<Employee> employeesskilled;

}
