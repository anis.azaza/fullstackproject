package com.FormationSpringBoot.GestionProduit.controlleur;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.FormationSpringBoot.GestionProduit.dao.EmployeeRepository;
import com.FormationSpringBoot.GestionProduit.entities.Skills;
import com.FormationSpringBoot.GestionProduit.service.IServiceEmployee;
import com.FormationSpringBoot.GestionProduit.service.IServiceSkills;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/skill")
@AllArgsConstructor
public class SkillController {
	IServiceSkills ss;
	IServiceEmployee se;
	EmployeeRepository er; 
	
	@GetMapping("/addskill")
	public String addSkill(Model m) {
		m.addAttribute("employee",se.getAllEmployees());
		return "addskill";
	}
	
	@PostMapping("/saveskill")
	public String saveSkill (@ModelAttribute Skills skill,Model m)  {
		Integer id =skill.getId();
		ss.saveSkill(skill);
		if(id!=null) {
			return "redirect:/skill/all";
		}
		else
		{
			m.addAttribute("message","Successufully Added");
			m.addAttribute("skill",se.getAllEmployees());
			return "saveskill";
		}
	}
	
	
	
	@GetMapping("/all")
	public String getAllSkill(Model m)
	{
		List<Skills>list=ss.getAllSkills();
		m.addAttribute("skill", list);
		m.addAttribute("employees", er.findAll());
		m.addAttribute("employees", "all employees");
		return "listskills";
		
	}
	

}
