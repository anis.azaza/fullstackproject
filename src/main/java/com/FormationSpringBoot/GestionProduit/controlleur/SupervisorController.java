package com.FormationSpringBoot.GestionProduit.controlleur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.FormationSpringBoot.GestionProduit.dao.DepartementRepository;
import com.FormationSpringBoot.GestionProduit.entities.Supervisor;
import com.FormationSpringBoot.GestionProduit.service.IServiceDepartement;
import com.FormationSpringBoot.GestionProduit.service.IServiceSupervisor;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/supervisor")
@AllArgsConstructor

public class SupervisorController {
	@Autowired
	IServiceSupervisor ss;
	@Autowired
	IServiceDepartement ds;
	@Autowired
	DepartementRepository dr;
	
	
	
	
	@GetMapping("/addsupervisor")
	public String addSupervisor(Model m) {
		m.addAttribute("department",ds.allDepartements());
		return "addsupervisor";
	}
	
	@PostMapping("/savesupervisor")
	public String saveSupervisor (@ModelAttribute Supervisor sup,Model m)  {
		Integer id =sup.getId();
		ss.saveSupervisor(sup);
		if(id!=null) {
			return "redirect:/supervisor/all";
		}
		else
		{
			m.addAttribute("message","Successufully Added");
			m.addAttribute("departements",ds.allDepartements());
			return "savesupervisor";
		}
	}
	
	
	
	@GetMapping("/all")
	public String getAllSupervisors(Model m)
	{
		List<Supervisor>list=ss.getAllSupervisor();
		m.addAttribute("supervisor", list);
		m.addAttribute("departement", dr.findAll());
		m.addAttribute("departements", "all departments");
		return "listsupervisors";
		
	}
	
	
	

}
