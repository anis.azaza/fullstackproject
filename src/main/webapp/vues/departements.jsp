<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Departements</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<%@include file="navbar.html"%>
<br><br>
<br><hr>
<table class="table table-hover">
<tr>
<th>Id</th><th>Name</th><th>Supervisor</th><th>Action</th>
</tr>
<c:forEach items="${departement}" var="dep">
<tr>
<td>${dep.id}</td>
<td>${dep.name}</td>
<td>${dep.supervisor.fname}</td>
<td><a href="/departement/delete/${dep.id}"
 class="btn btn-danger">Delete</a>
 <a href="/departement/update/${dep.id}" class="btn btn-warning">
 Update</a></td>
</tr>
</c:forEach>
</table>
</body>
</html>