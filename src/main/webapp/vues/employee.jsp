<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<%@include file="navbar.html"%>
<br><br>
<form action="/employee/employeesbydepartements" method="get">
<div class="mb-3">
<select class="form-select" name="departement" onChange="submit()">
<option selected hidden>${departement}</option>
<option value="0"> all departements</option>
<c:forEach items="${departements}" var="cc">
<option value="${cc.id}">${cc.name}</option> </c:forEach>
</select>
</div>
</form>
<br><hr>
<table class="table table-hover">
<tr>
<th>Id</th><th>fName</th><th>lName</th><th>email</th><th>Age</th><th>Departement</th><th>Action</th>
</tr>
<c:forEach items="${employee}" var="emp">
<tr>
<td>${emp.id}</td>
<td>${emp.fname}</td>
<td>${emp.lname}</td>
<td>${emp.email}</td>
<td>${emp.age}</td>
<td>${emp.departement.name}</td>
<td><a href="/employee/delete/${emp.id}"
 class="btn btn-danger">Delete</a>
 <a href="/employee/update/${emp.id}" class="btn btn-warning">
 Update</a></td>
</tr>
</c:forEach>
</table>
</body>
</html>